/* RoboGuitar controller code
 * SENG 466 Assignment 1
 * University of Victoria
 * By Connor Sauve
 */
#include <Servo.h>

//actions
#define ACTION_CONTINUE 0
#define ACTION_PLAY 1
#define ACTION_STOP 2
#define ACTION_TUNE 3
#define ACTION_SET_PERIOD 4
#define ACTION_SET_TENSION 5

//states
#define STATE_STOPPED 1
#define STATE_PLAYING 2
#define STATE_TUNING 3

//freq sensor
#define PIN_FREQ_SENSOR_OUT 2

//tension stepper motor
#define PIN_STEPPER_1 8
#define PIN_STEPPER_2 10
#define PIN_STEPPER_3 9
#define PIN_STEPPER_4 11
#define STEP_DELAY_MICRO 50000  //delay between each step in the below sequence
char stepperSequence[8] = {B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001};
int targetTensionDirection = 0; //negative = tighter, positive = looser

/* MG995 Servo
 * limitations:
 *   - Max rotation angle: 0-200 degree, which maps to 0-165 Servo Lib angle (1-180 scale)
 *   - Constant rotation speed: 60degree/200ms
 *   - max wait of 200 degree is 667ms (from 0 to 200 degree)
 */
#define PIN_SERVO 12
#define MIN_PLUCK_PERIOD_MS 100
#define MAX_PLUCK_PERIOD_MS 2000
#define SERVO_MIN_ANGLE 0
#define SERVO_MAX_ANGLE 165
#define PLUCK_ANGLE_A_SERVOLIB 85
#define PLUCK_ANGLE_B_SERVOLIB 125

Servo servo;
bool pluckSide = false;
int millisToNextPluck = 1000;
int pluckPeriodMillis = 1000;

//frequency detection
#define TUNING_TARGET_PERIOD 120
#define TUNING_MIN_PERIOD 90
#define TUNING_MAX_PERIOD 190
#define TUNING_PLUCK_PERIOD 1000
byte newData = 0;
byte prevData = 0;
unsigned int time = 0;//keeps time and sends vales to store in timer[] occasionally
int timer[10];//sstorage for timing of events
int slope[10];//storage fro slope of events
unsigned int totalTimer;//used to calculate period
unsigned int period;//storage for period of wave
byte index = 0;//current storage index
int maxSlope = 0;//used to calculate max slope as trigger point
int newSlope;//storage for incoming slope data
//variables for decided whether you have a match
byte noMatch = 0;//counts how many non-matches you've received to reset variables if it's been too long
byte slopeTol = 3;//slope tolerance- adjust this if you need
int timerTol = 10;//timer tolerance- adjust this if you need

//global vars
#define BLUETOOTH_BUFFER_SIZE 20
char bluetoothBuffer[BLUETOOTH_BUFFER_SIZE] = {0};
short bluetoothBufferPos = 0;
short state;
short param = 0;
int lastTime;

//helper function
boolean inRange(int value, int minValue, int maxValue) {
  return value >= minValue && value <= maxValue;
}

void setup() {
  //setup pins
  servo.attach(PIN_SERVO);
  pinMode(PIN_STEPPER_1, OUTPUT);
  pinMode(PIN_STEPPER_2, OUTPUT);
  pinMode(PIN_STEPPER_3, OUTPUT);
  pinMode(PIN_STEPPER_4, OUTPUT);
  //pinMode(PIN_FREQ_SENSOR_OUT, INPUT);
  
  //interrupts
  cli();
  ADCSRA = 0;
  ADCSRB = 0;
  ADMUX |= (1 << REFS0); //set reference voltage
  ADMUX |= (1 << ADLAR); //left align the ADC value- so we can read highest 8 bits from ADCH register only
  ADCSRA |= (1 << ADPS2) | (1 << ADPS0); //set ADC clock with 32 prescaler- 16mHz/32=500kHz
  ADCSRA |= (1 << ADATE); //enabble auto trigger
  ADCSRA |= (1 << ADIE); //enable interrupts when measurement complete
  ADCSRA |= (1 << ADEN); //enable ADC
  ADCSRA |= (1 << ADSC); //start ADC measurements
  sei();
  
  //bluetooth...
  Serial.begin(57600);
  delay(150);
  Serial.println("Bluetooth serial initialized");
  
  //init
  lastTime = millis();
  state = STATE_STOPPED;
}

void resetISR(){//clear out some variables
  index = 0;//reset index
  noMatch = 0;//reset match couner
  maxSlope = 0;//reset slope
}

/* Interrupt handler for the freq sensor
 *
 * Generalized wave freq detection with 38.5kHz sampling rate and interrupts
 * by Amanda Ghassaei 2012
 * http://www.instructables.com/id/Arduino-Frequency-Detection/
 */
ISR(ADC_vect) {
  prevData = newData; //store previous value
  newData = ADCH; //get value from A0
  if (prevData < 127 && newData >= 127) { //if increasing and crossing midpoint
    newSlope = newData - prevData; //calculate slope
    if (abs(newSlope - maxSlope) < slopeTol) { //if slopes are ==
      //record new data and reset time
      slope[index] = newSlope;
      timer[index] = time;
      time = 0;
      if (index == 0) { //new max slope just reset
        noMatch = 0;
        index++; //increment index
      }
      else if (abs(timer[0] - timer[index]) < timerTol &&
               abs(slope[0] - newSlope) < slopeTol) { //if timer duration and slopes match
        //sum timer values
        totalTimer = 0;
        for (byte i = 0; i < index; i++) {
          totalTimer+=timer[i];
        }
        period = totalTimer; //set period
        //reset new zero index values to compare with
        timer[0] = timer[index];
        slope[0] = slope[index];
        index = 1; //set index to 1
        noMatch = 0;
      }
      else { //crossing midpoint but not match
        index++;
        if (index > 9) {
          resetISR();
        }
      }
    } else if (newSlope > maxSlope) { //if new slope is much larger than max slope
      maxSlope = newSlope;
      time = 0;//reset clock
      noMatch = 0;
      index = 0;//reset index
    } else { //slope not steep enough
      noMatch++;//increment no match counter
      if (noMatch > 9) {
        resetISR();
      }
    }
  }
  time++; //increment timer at rate of 38.5kHz
}

//safely sets the pluck period within the bounds
void setPluckPeriod(int period) {
  if (period < MIN_PLUCK_PERIOD_MS) {
    period = MIN_PLUCK_PERIOD_MS;
  } else if (period > MAX_PLUCK_PERIOD_MS) {
    period = MAX_PLUCK_PERIOD_MS;
  }
  pluckPeriodMillis = period;
}

//turn the tension stepper 1 step in a given direction every STEP_DELAY_MS while continuously called
void turnStepper(bool turnDirection) {
  //looser = true, tighter = false
  //assume at step 0
  int startStepIndex = 8;
  for (int i = 1; i <= 8; i++) {
    int stepIndex = (startStepIndex + (turnDirection ? i : -i)) % 8;
    
    digitalWrite(PIN_STEPPER_1, bitRead(stepperSequence[stepIndex], 0));
    digitalWrite(PIN_STEPPER_2, bitRead(stepperSequence[stepIndex], 1));
    digitalWrite(PIN_STEPPER_3, bitRead(stepperSequence[stepIndex], 2));
    digitalWrite(PIN_STEPPER_4, bitRead(stepperSequence[stepIndex], 3));
    delayMicroseconds(STEP_DELAY_MICRO);
  }
}

void adjustTension() {
  if (targetTensionDirection != 0) {
    turnStepper(targetTensionDirection > 0);
    targetTensionDirection = 0;
  }
}

void setServoAngle(int angle) {
  if (angle < SERVO_MIN_ANGLE) {
    angle = SERVO_MIN_ANGLE;
  } else if (angle > SERVO_MAX_ANGLE) {
    angle = SERVO_MAX_ANGLE;
  }
  servo.write(angle);
}

//move the servo to one of the two pluck angles, alternating each time called
void pluck() {
  setServoAngle(pluckSide ? PLUCK_ANGLE_A_SERVOLIB : PLUCK_ANGLE_B_SERVOLIB);
  pluckSide = !pluckSide;
}

void tune(int elapsedTimeMillis) {
  if (inRange(period, TUNING_TARGET_PERIOD - 2, TUNING_TARGET_PERIOD + 2)) {
    state = STATE_STOPPED;
    return;
  }
  
  millisToNextPluck -= elapsedTimeMillis;
  if (millisToNextPluck <= 0) {
    //it's time to pluck...
    millisToNextPluck += pluckPeriodMillis;
    pluck();
    turnStepper(period < TUNING_TARGET_PERIOD);
  } 
}

void play(int elapsedTimeMillis) {
  millisToNextPluck -= elapsedTimeMillis;
  if (millisToNextPluck <= 0) {
    //it's time to pluck...
    millisToNextPluck += pluckPeriodMillis;
    pluck();
  }
  adjustTension();
}

void stopped() {
  adjustTension(); 
}

//branches execution based on what state the state machine is in
void runState(int elapsedTimeMillis) {
  switch (state) {
    case STATE_PLAYING:
      play(elapsedTimeMillis);
      break;
    case STATE_STOPPED:
      stopped();
      break;
    case STATE_TUNING:
      tune(elapsedTimeMillis);
      break;
  }
}

//performs an action, such as setting parameters or transitioning to states
void doAction(int action, int param) {
  switch(action) {
    case ACTION_CONTINUE:
      break;
    case ACTION_SET_TENSION:
      targetTensionDirection = param;
      break;
    case ACTION_SET_PERIOD:
      setPluckPeriod(param);
      break;
    case ACTION_PLAY:
      state = STATE_PLAYING;
      break;
    case ACTION_STOP:
      state = STATE_STOPPED;
      break;
    case ACTION_TUNE:
      pluck(); //get the period initialized
      pluckPeriodMillis = TUNING_PLUCK_PERIOD;
      state = STATE_TUNING;
      break;
  } 
}

//main loop, called at an unknown/unreliable frequency
void loop() {
  int action = ACTION_CONTINUE;
  int param;
  
  //read commands from bluetooth
  while (Serial.available()) {
      char inChar = (char)Serial.read();
      if (inChar == '\n') {
        bluetoothBuffer[bluetoothBufferPos] = '\0';
        Serial.print("Received command: " );
        Serial.println(bluetoothBuffer);
        
        if (!strncmp(bluetoothBuffer, "play", 4)) {
          action = ACTION_PLAY;
        } else if (!strncmp(bluetoothBuffer, "stop", 4)) {
          action = ACTION_STOP;
        } else if (!strncmp(bluetoothBuffer, "set_period ", 11)) {
          action = ACTION_SET_PERIOD;
          param = atoi(bluetoothBuffer + 11);
        } else if (!strncmp(bluetoothBuffer, "set_tension ", 12)) {
          action = ACTION_SET_TENSION;
          param = atoi(bluetoothBuffer + 12);
        } else if (!strncmp(bluetoothBuffer, "tune", 4)) {
          action = ACTION_TUNE;
        }
        
        bluetoothBufferPos = 0;
      } else {
        bluetoothBuffer[bluetoothBufferPos++] = inChar;
      }
  }
  
  //transitions and settings
  doAction(action, param);
  
  //run the state
  int now = millis();
  int elapsedTime = now - lastTime;
  lastTime = now;
  runState(elapsedTime);
}
